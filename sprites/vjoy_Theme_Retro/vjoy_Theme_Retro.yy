{
    "id": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "vjoy_Theme_Retro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 7,
    "bbox_right": 152,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ae177b7-7ea8-4c39-9574-4735b90b2c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
            "compositeImage": {
                "id": "25a3e943-8151-4f48-882b-6a1396215d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae177b7-7ea8-4c39-9574-4735b90b2c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce15df0a-e24f-4b19-af69-d5f15e91d8b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae177b7-7ea8-4c39-9574-4735b90b2c91",
                    "LayerId": "e364f19a-5e24-45e9-b11f-b4b60e299beb"
                }
            ]
        },
        {
            "id": "38cd67ab-0026-4a7c-bb82-9df81cb4f916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
            "compositeImage": {
                "id": "19677cb9-c941-48ac-a2ed-a0655a102c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38cd67ab-0026-4a7c-bb82-9df81cb4f916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591e6ab0-97c1-4726-99d9-971f2915b65c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38cd67ab-0026-4a7c-bb82-9df81cb4f916",
                    "LayerId": "e364f19a-5e24-45e9-b11f-b4b60e299beb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "e364f19a-5e24-45e9-b11f-b4b60e299beb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}