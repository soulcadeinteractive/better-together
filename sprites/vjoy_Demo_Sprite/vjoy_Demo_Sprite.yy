{
    "id": "74b3e227-f51b-4f16-9faa-e6e3b0f4cc4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "vjoy_Demo_Sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb1d0504-f748-4f54-ae58-e5d08e84bb43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b3e227-f51b-4f16-9faa-e6e3b0f4cc4d",
            "compositeImage": {
                "id": "8681a795-d9f9-4c7c-96aa-0e44ae1fd91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1d0504-f748-4f54-ae58-e5d08e84bb43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18507320-6230-4b40-ac34-d459f4455edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1d0504-f748-4f54-ae58-e5d08e84bb43",
                    "LayerId": "1a50eee2-7b2b-48c7-85bc-9a8a3d23d50c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1a50eee2-7b2b-48c7-85bc-9a8a3d23d50c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74b3e227-f51b-4f16-9faa-e6e3b0f4cc4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}