{
    "id": "b989281b-64d3-475c-9343-b2e380403d5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RightStick",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "38df43dd-07e0-4767-af3f-1229f7da189c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
    "visible": true
}