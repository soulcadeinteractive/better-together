{
    "id": "4fb90024-fcb8-4dc6-a64d-8854b7bd2fef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_LeftStick",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "38df43dd-07e0-4767-af3f-1229f7da189c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "331e293c-5cf3-4dc6-a491-ad02b87efaa3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        },
        {
            "id": "f321220d-495b-4ee9-b775-dd369b5f439c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7eed97a-f555-4a90-b4e2-0f2d3679c875",
    "visible": true
}