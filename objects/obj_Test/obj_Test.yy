{
    "id": "618aa9c8-78f0-444b-821f-60116158fe22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Test",
    "eventList": [
        {
            "id": "0b82f2ae-717f-4d37-b709-5e60f04dea91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "618aa9c8-78f0-444b-821f-60116158fe22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "8fc41801-0e1f-4d2a-bac5-d8c67cec5d5d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "760bb208-58a5-4394-9cb9-7eba4aaf6b26",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "74b3e227-f51b-4f16-9faa-e6e3b0f4cc4d",
    "visible": true
}