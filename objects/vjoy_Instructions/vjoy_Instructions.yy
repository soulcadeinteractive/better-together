{
    "id": "ef61e39d-cdd8-4ef5-9c65-c8074a5cf73d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vjoy_Instructions",
    "eventList": [
        {
            "id": "1f5b0487-6ef7-480d-b3e4-c415c4bd4ffb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef61e39d-cdd8-4ef5-9c65-c8074a5cf73d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "f4cf861b-2cee-4dcd-90a6-f50d4d59dd01",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        },
        {
            "id": "5994e03a-f20d-4ee3-b5a7-481eb140ce3f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e45fdb78-5095-49bb-b709-b49b67f6ee9e",
    "visible": true
}